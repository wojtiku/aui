function OutputCtrl($scope) {
	var iframeWindow, iframeDocument;
	var $head, $body, $css, $js;

  $scope.soyCompileTimeout = null;

	AJS.$('body').bind('runJavascript', function() {
		$scope.outputAndRunJs();
	});

	$scope.bindToEditorChange(null, function() {
		$scope.output();
	});

	$scope.reset = function() {
		// Get iframe document
		iframeWindow = AJS.$('#output-frame')[0].contentWindow;
		iframeDocument = iframeWindow.document;
		iframeDocument.open(); iframeDocument.close(); // must open and close document object to start using it!

		// Containers to dump code into
		$head = AJS.$("head", iframeDocument);
		$body = AJS.$("body", iframeDocument).addClass("aui-theme-default").addClass("aui-layout");
        // Inject AUI resources
        $head.append('<link rel="stylesheet" type="text/css" href="static/aui/css/aui-all.css">');
		$css = AJS.$("<style>").appendTo($head);
		$js = null; // JS is special. Can't just dump into the same <script> tag and expect it to run.

		// Can't use jQuery here as it will try to load the script with XmlHttpRequest
		var script = iframeDocument.createElement('script');
		script.type = 'text/javascript';
		script.src = 'static/aui/js/aui-all.js';
		$head[0].appendChild(script);

        script = iframeDocument.createElement('script');
        script.type = 'text/javascript';
        script.src = 'static/js/libraries/soyutils.js';
        $head[0].appendChild(script);
	};

	$scope.output = function() {
		var html = $scope.editors.html.getValue();
		var css = $scope.editors.css.getValue();

	    try {
            if($scope.soySupporter.enable) {
                $scope.handleSoyRequestThrottle();
            } else {
                $body.html(html);
            }
            $css.html(css);
	    } catch(e) {
	    	// Ignore
	    }
	};

    $scope.handleSoyRequestThrottle = function() {
        if ($scope.soyCompileTimeout) {
            clearTimeout($scope.soyCompileTimeout);
            $scope.soyCompileTimeout = null;
        }
        /*
         Send soy compile request every 1.5 seconds
         */
        $scope.soyCompileTimeout = setTimeout(function() {
            clearTimeout($scope.soyCompileTimeout);
            $scope.soySupporter.insertSoyContents({
                editors: {
                    soy: $scope.editors.soy.getValue(),
                    html: $scope.editors.html.getValue(),
                    js: $scope.editors.js.getValue()
                },
                iframe: {
                    iframeWindow: iframeWindow,
                    iframeDocument: iframeDocument,
                    $head: $head
                }
            });
        }, $scope.soySupporter.soyThrottleRate);
    }

	$scope.outputAndRunJs = function() {
		$scope.reset();
		$scope.output();

		var js = $scope.editors.js.getValue();
		if ($js) $js.remove();
		$js = iframeWindow.AJS.$('<script>').html(js).appendTo($body);
	};

	$scope.reset();
}