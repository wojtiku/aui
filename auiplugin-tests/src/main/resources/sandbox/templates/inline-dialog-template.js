AJS.InlineDialog(AJS.$("#popupLink"), 1, "templates/inline-dialog-content.html");

/* 
You can also pass content in directly:

AJS.InlineDialog(AJS.$("#popupLink"), 2, 
    function(content, trigger, showPopup) {
        content.css({"padding":"16px"}).html('<p>Appended asdf content.</p>');
        showPopup();
        return false;
    }
);

*/