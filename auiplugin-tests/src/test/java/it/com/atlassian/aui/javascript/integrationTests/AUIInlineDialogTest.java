package it.com.atlassian.aui.javascript.integrationTests;

import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.WebDriverElement;
import com.atlassian.pageobjects.elements.query.Conditions;
import com.atlassian.webdriver.utils.JavaScriptUtils;
import it.com.atlassian.aui.javascript.AbstractAuiIntegrationTest;
import it.com.atlassian.aui.javascript.pages.InlineDialogTestPage;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import static org.junit.Assert.assertTrue;

public class AUIInlineDialogTest extends AbstractAuiIntegrationTest
{
    private InlineDialogTestPage inlineDialogTestPage;
    private PageElementFinder elementFinder;

    @Before
    public void setup()
    {
        inlineDialogTestPage = product.visit(InlineDialogTestPage.class);
        elementFinder = inlineDialogTestPage.getElementFinder();
    }

    //test that a standard InlineDialog shows on click
    @Test
    public void testAUIInlineDialogStandardClickShow()
    {
        PageElement dialog = openInlineDialogWithClick(By.id("popupLink"), By.id("inline-dialog-1"));
        assertTrue("Inline dialog should be present", dialog.isPresent());
        assertTrue("Inline dialog should be visible", dialog.isVisible());
    }

    //test that a standard hover inline-dialog show on hover
    @Test
    public void testAUIInlineDialogStandardHoverShow()
    {
         PageElement dialog = openInlineDialogWithHover(By.id("hoverLink"), By.id("inline-dialog-2"));
         assertTrue("Inline dialog should be present", dialog.isPresent());
         assertTrue("Inline dialog should be visible", dialog.isVisible());
    }

    //test noBind
    @Test
    public void testAUIInlineDialogNoBindOption()
    {
        PageElement trigger = elementFinder.find(By.id("noBind")).click();
        PageElement dialog = elementFinder.find(By.id("inline-dialog-3"));

        assertTrue("Inline Dialog should not be present", Conditions.not(dialog.timed().isPresent()).now());
        assertTrue("Inline Dialog should not be visible", Conditions.not(dialog.timed().isVisible()).now());
    }

    //test noBind with manual Binding
    @Test
    @Ignore("Flaky test fails on buildbox")
    public void testAUIInlineDialogNoBindOptionWithManualBinding()
    {
        PageElement dialog = openInlineDialogWithClick(By.id("testNoBind2"), By.id("inline-dialog-16"));
        assertTrue(dialog.isPresent());
        assertTrue(dialog.isVisible());
    }

    //test right positioned short trigger
    @Test
    public void testRightPositionedShortTrigger()
    {
        WebDriverElement dialog = (WebDriverElement) openInlineDialogWithClick(By.id("testFloat"), By.id("inline-dialog-5"));
        assertTrue(dialog.isPresent());
        assertTrue(dialog.isVisible());

        int expectedPosition = (int) (JavaScriptUtils.execute(Long.class, "return window.innerWidth", product.getTester().getDriver()) - 5 -
                dialog.asWebElement().getSize().getWidth());

        int actualPosition = dialog.asWebElement().getLocation().getX();

        assertTrue("right positioned inline-dialog is not positioned correctly", isWithinRange(expectedPosition, actualPosition, 22));
    }

    //test right positioned medium trigger
    @Test
    public void testRightPositionedMediumTrigger()
    {
        WebDriverElement dialog = (WebDriverElement) openInlineDialogWithClick(By.id("testFloat3"), By.id("inline-dialog-7"));
        assertTrue(dialog.isPresent());
        assertTrue(dialog.isVisible());

        int expectedPosition = (int) (JavaScriptUtils.execute(Long.class, "return window.innerWidth", product.getTester().getDriver()) - 5 -
                dialog.asWebElement().getSize().getWidth());

        int actualPosition = dialog.asWebElement().getLocation().getX();

        assertTrue("right positioned inline dialog is not positioned correctly", isWithinRange(expectedPosition, actualPosition, 22));
    }

    //test right positioned Long trigger
    @Test
    public void testRightPositionedLongTrigger()
    {
        WebDriverElement dialog = (WebDriverElement) openInlineDialogWithClick(By.id("testFloat2"), By.id("inline-dialog-6"));
        assertTrue(dialog.isPresent());
        assertTrue(dialog.isVisible());

        int expectedPosition = ((WebDriverElement) elementFinder.find(By.id("testFloat2"))).asWebElement().getLocation().getX();
        int actualPosition = dialog.asWebElement().getLocation().getX();

        assertTrue("right positioned inline-dialog is not positioned correctly", isWithinRange(expectedPosition, actualPosition,  20));
    }

    private PageElement openInlineDialogWithHover(By triggerLocator, By dialogLocator)
    {
        WebElement trigger = ((WebDriverElement) elementFinder.find(triggerLocator)).asWebElement();

        new Actions(product.getTester().getDriver()).moveToElement(trigger).perform();

        PageElement dialog = elementFinder.find(dialogLocator);

        dialog.timed().isVisible().now();

        return dialog;
    }

    private PageElement openInlineDialogWithClick(By triggerLocator, By dialogLocator)
    {
        elementFinder.find(triggerLocator).click();
        PageElement dialog = elementFinder.find(dialogLocator);

        dialog.timed().isVisible().now();

        return dialog;
    }

}
