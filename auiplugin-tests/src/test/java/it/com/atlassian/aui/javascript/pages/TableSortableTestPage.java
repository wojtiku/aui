package it.com.atlassian.aui.javascript.pages;

/**
 * Provides the test page for the table sortable test.
 */
public class TableSortableTestPage extends TestPage {
    public String getUrl()
    {
        return "/plugins/servlet/ajstest/test-pages/experimental/tables-sortable/";
    }
}
