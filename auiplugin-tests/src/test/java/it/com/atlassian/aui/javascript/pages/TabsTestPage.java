package it.com.atlassian.aui.javascript.pages;

/**
 * @since 4.0
 */
public class TabsTestPage extends TestPage
{
    public String getUrl()
    {
        return "/plugins/servlet/ajstest/test-pages/tabs/tabs-test.html";
    }
}
