<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <artifactId>auiplugin-parent</artifactId>
        <groupId>com.atlassian.aui</groupId>
        <version>5.3-SNAPSHOT</version>
    </parent>

    <artifactId>auiplugin</artifactId>
    <packaging>atlassian-plugin</packaging>

    <name>Atlassian UI Plugin</name>
    <description>An Atlassian plugin that contains the core javascript files used in Atlassian products.</description>
    <url>https://bitbucket.org/atlassian/aui</url>

    <properties>
        <atlassian.plugin.key>com.atlassian.auiplugin</atlassian.plugin.key>
        <maven.build.timestamp.format>yyyy.MM.dd HH:mm:ss Z</maven.build.timestamp.format>
        <!-- Workaround from http://jira.codehaus.org/browse/MRESOURCES-99
             Used for ${timestamp} replacements later -->
        <timestamp>${maven.build.timestamp}</timestamp>
    </properties>

    <dependencies>
        <dependency>
            <groupId>com.atlassian.aui</groupId>
            <artifactId>auiplugin-spi</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>dom4j</groupId>
            <artifactId>dom4j</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.plugins</groupId>
            <artifactId>atlassian-plugins-osgi</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.plugins</groupId>
            <artifactId>atlassian-plugins-webresource</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.soy</groupId>
            <artifactId>soy-template-renderer-api</artifactId>
            <scope>provided</scope>
        </dependency>

        <!-- Test dependencies -->
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.mockito</groupId>
            <artifactId>mockito-all</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.core</groupId>
            <artifactId>atlassian-core</artifactId>
            <scope>test</scope>
        </dependency>
        <!-- Only exists to unpack soy deps for testing -->
        <dependency>
            <groupId>com.atlassian.soy</groupId>
            <artifactId>soy-template-plugin</artifactId>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <build>
        <resources>
            <!-- This filtering strategy is intentionally weird to trick the IDEA plugin into including the directory in the classpath -->
            <resource>
                <directory>src/main/resources</directory>
                <excludes>
                    <exclude>atlassian-plugin.xml</exclude>
                </excludes>
            </resource>
            <resource>
                <!-- This defines project.version transform targets. -->
                <directory>src/main/resources</directory>
                <filtering>true</filtering>
                <includes>
                    <include>atlassian-plugin.xml</include>
                    <include>version</include>
                    <include>js/atlassian/atlassian.js</include>
                    <include>old/js/atlassian/atlassian.js</include>
                </includes>
            </resource>
        </resources>

        <plugins>
            <plugin>
                <groupId>com.atlassian.maven.plugins</groupId>
                <artifactId>qunit-maven-plugin</artifactId>
                <executions>
                    <execution>
                        <phase>test</phase>
                        <goals>
                            <goal>test</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <excludes>
                        <exclude>${js-test-exclude}</exclude>
                    </excludes>
                    <systemPropertyVariables>
                        <xvfb.enable>${xvfb.enable}</xvfb.enable>
                        <webdriver.browser>${webdriver.browser}</webdriver.browser>
                    </systemPropertyVariables>
                    <resourceLocations>
                        <resourceLocation>${project.basedir}/src/main/resources</resourceLocation>
                        <resourceLocation>${project.basedir}/src/test/resources</resourceLocation>
                        <resourceLocation>${project.build.directory}/qunit</resourceLocation>
                    </resourceLocations>
                </configuration>
            </plugin>
            <plugin>
                <groupId>com.atlassian.maven.plugins</groupId>
                <artifactId>maven-amps-plugin</artifactId>
                <configuration>
                    <productVersion>${refapp.version}</productVersion>
                    <extractDependencies>true</extractDependencies>
                    <compressResources>false</compressResources> <!-- we invoke YUI explicitly below -->
                </configuration>
            </plugin>
            <!--START - Minification -->
            <plugin>
                <groupId>net.sf.alchim</groupId>
                <artifactId>yuicompressor-maven-plugin</artifactId>
                <executions>
                    <execution>
                        <goals>
                            <goal>compress</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <!-- Everything on one line -->
                    <linebreakpos>-1</linebreakpos>
                    <!-- Turning off JSlint warnings -->
                    <jswarn>false</jswarn>
                    <excludes>
                        <exclude>*.xml</exclude>
                        <exclude>**/src/test/*</exclude>
                        <exclude>**/src/samples/*</exclude>
                    </excludes>
                </configuration>
            </plugin>
            <!--END - Minification -->
            <!-- Unpacks dependencies so that we can get soyutils.js from qunit tests -->
            <plugin>
                <artifactId>maven-dependency-plugin</artifactId>
                <executions>
                    <execution>
                        <id>extract-soyutils-dependency</id>
                        <phase>generate-test-resources</phase>
                        <goals>
                            <goal>unpack-dependencies</goal>
                        </goals>
                        <configuration>
                            <outputDirectory>${project.build.directory}/qunit/dependencies</outputDirectory>
                            <includeGroupIds>com.atlassian.soy</includeGroupIds>
                            <includeArtifactIds>soy-template-plugin</includeArtifactIds>
                            <includes>**/soyutils.js</includes>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <!-- Compiles js to soy -->
            <plugin>
                <groupId>com.atlassian.maven.plugins</groupId>
                <artifactId>soy-to-js-maven-plugin</artifactId>
                <executions>
                    <execution>
                        <id>generate-js-from-soy</id>
                        <phase>generate-test-resources</phase>
                        <goals>
                            <goal>compile</goal>
                        </goals>
                        <configuration>
                            <outputDirectory>${project.build.directory}/qunit/soy</outputDirectory>
                            <resources>
                                <directory>${project.basedir}/src/main/resources/soy/atlassian/</directory>
                                <includes>
                                    <include>**/*.soy</include>
                                </includes>
                            </resources>
                            <propertiesFile>${project.basedir}/src/main/resources/auiplugin.properties</propertiesFile>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>
</project>
