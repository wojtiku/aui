package com.atlassian.aui.soy;

import com.atlassian.soy.renderer.JsExpression;
import com.atlassian.soy.renderer.SoyClientFunction;
import com.atlassian.soy.renderer.SoyServerFunction;
import com.google.common.collect.ImmutableSet;

import java.util.Set;

public class isListFunction implements SoyServerFunction<Boolean>, SoyClientFunction {

    @Override
    public String getName() {
        return "isList";
    }

    @Override
    public JsExpression generate(JsExpression... jsExpressions) {
        return new JsExpression("(" + jsExpressions[0].getText() + ") instanceof Array");
    }

    @Override
    public Boolean apply(Object... objects) {
        return objects[0] instanceof Iterable;
    }

    @Override
    public Set<Integer> validArgSizes() {
        return ImmutableSet.of(1);
    }
}
