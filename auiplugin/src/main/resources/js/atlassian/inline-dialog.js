(function($) {
    /**
     * Creates a new inline dialog.
     *
     * @class InlineDialog
     * @namespace AJS
     * @constructor
     * @param items jQuery object - the items that trigger the display of this popup when the user mouses over.
     * @param identifier A unique identifier for this popup. This should be unique across all popups on the page and a valid CSS class.
     * @param url The URL to retrieve popup contents.
     * @param options Custom options to change default behaviour. See AJS.InlineDialog.opts for default values and valid options.
     */
    AJS.InlineDialog = function (items, identifier, url, options) {
        if(options && options.getArrowAttributes){
            AJS.log("DEPRECATED: getArrowAttributes - See https://ecosystem.atlassian.net/browse/AUI-1362");
        }

        if(options && options.getArrowPath){
            AJS.log("DEPRECATED: getArrowPath - See https://ecosystem.atlassian.net/browse/AUI-1362");
        }

        // attempt to generate a random identifier if it doesn't exist
        if (typeof identifier === 'undefined') {

            identifier = String(Math.random()).replace('.', '');

            // if the generated supplied identifier already exists when combined with the prefixes we'll be using, then bail
            if ($('#inline-dialog-' + identifier + ', #arrow-' + identifier + ', #inline-dialog-shim-' + identifier).length) {
                throw 'GENERATED_IDENTIFIER_NOT_UNIQUE';
            }

        }

        var opts = $.extend(false, AJS.InlineDialog.opts, options);
        var renderAsSVG = function() {
            return window.Raphael && options && (options.getArrowPath || options.getArrowAttributes);
        };

        var hash;
        var hideDelayTimer;
        var showTimer;
        var beingShown = false;
        var shouldShow = false;
        var contentLoaded = false;
        var mousePosition;
        var targetPosition;
        var popup  = $('<div id="inline-dialog-' + identifier + '" class="aui-inline-dialog"><div class="contents"></div><div id="arrow-' + identifier + '" class="arrow"></div></div>');
        var arrow = $("#arrow-" + identifier, popup);
        var contents = popup.find(".contents");

        if (!renderAsSVG()) {
            popup.find(".arrow").addClass("aui-css-arrow");
        }

        contents.css("width", opts.width + "px");
        contents.mouseover(function(e) {
            clearTimeout(hideDelayTimer);
            popup.unbind("mouseover");
            //e.stopPropagation();
        }).mouseout(function() {
            hidePopup();
        });

        var getHash = function () {
            if (!hash) {
                hash = {
                    popup: popup,
                    hide: function(){
                        hidePopup(0);
                    },
                    id: identifier,
                    show: function(){
                        showPopup();
                    },
                    persistent: opts.persistent ? true : false,
                    reset: function () {

                        function drawPopup (popup, positions) {
                            //Position the popup using the left and right parameters
                            popup.css(positions.popupCss);

                            if (renderAsSVG()) {
                                //special adjustment for downards raphael arrow
                                if(positions.displayAbove){
                                    positions.arrowCss.top -= $.browser.msie ? 10 : 9;
                                }

                                if (!popup.arrowCanvas) {
                                    popup.arrowCanvas = Raphael("arrow-"+identifier, 16, 16);  //create canvas using arrow element
                                }
                                var getArrowPath = opts.getArrowPath,
                                    arrowPath = $.isFunction(getArrowPath) ?
                                                    getArrowPath(positions) :
                                                    getArrowPath;
                                //draw the arrow
                                popup.arrowCanvas
                                        .path(arrowPath)
                                        .attr(opts.getArrowAttributes());

                            } else {
                                //flip the arrow with a class if we need to, otherwise make sure it's pointing up
                                if(positions.displayAbove && !arrow.hasClass("aui-bottom-arrow")){
                                    arrow.addClass("aui-bottom-arrow")
                                } else if (!positions.displayAbove) {
                                    arrow.removeClass("aui-bottom-arrow");
                                }
                            }


                            arrow.css(positions.arrowCss);
                        }

                        //DRAW POPUP
                        var positions = opts.calculatePositions(popup, targetPosition, mousePosition, opts);

                        drawPopup (popup, positions);

                        // reset position of popup box
                        popup.fadeIn(opts.fadeTime, function() {
                            // once the animation is complete, set the tracker variables
                            // beingShown = false; // is this necessary? Maybe only the shouldShow will have to be reset?
                        });

                        if ($.browser.msie && ~~($.browser.version) < 10) {
                            // iframeShim, prepend if it doesnt exist
                            var jQueryCache = $('#inline-dialog-shim-'+identifier);
                            if(jQueryCache.length ==0){
                                $(popup).prepend($('<iframe class = "inline-dialog-shim" id="inline-dialog-shim-'+identifier+'" frameBorder="0" src="javascript:false;"></iframe>'));
                            }
                            //addjust height and width of shim according to the popup
                            jQueryCache.css({
                                width: contents.outerWidth(),
                                height: contents.outerHeight()
                            });
                        }
                    }
                };
            }
            return hash;
        };

        var showPopup = function() {
            if (popup.is(":visible")) {
                return;
            }
            showTimer = setTimeout(function() {
                if (!contentLoaded || !shouldShow) {
                    return;
                }
                opts.addActiveClass && $(items).addClass("active");
                beingShown = true;
                !opts.persistent && bindHideOnExternalClick();
                AJS.InlineDialog.current = getHash();
                $(document).trigger("showLayer", ["inlineDialog", getHash()]);
                // retrieve the position of the click target. The offsets might be different for different types of targets and therefore
                // either have to be customisable or we will have to be smarter about calculating the padding and elements around it

                getHash().reset();

            }, opts.showDelay);
        };

        var hidePopup = function(delay) {
            // do not auto hide the popup if persistent is set as true
            if (typeof delay == 'undefined' && opts.persistent) return;

            shouldShow = false;
            // only exectute the below if the popup is currently being shown
            // and the arbitrator callback gives us the green light
            if (beingShown && opts.preHideCallback.call(popup[0].popup)) {
                delay = (delay == null) ? opts.hideDelay : delay;
                clearTimeout(hideDelayTimer);
                clearTimeout(showTimer);
                // store the timer so that it can be cleared in the mouseover if required
                //disable auto-hide if user passes null for hideDelay
                if (delay != null) {
                    hideDelayTimer = setTimeout(function() {
                        unbindHideOnExternalClick();
                        opts.addActiveClass && $(items).removeClass("active");
                        popup.fadeOut(opts.fadeTime, function() { opts.hideCallback.call(popup[0].popup); });
                        //If there's a raphael arrow remove it properly
                        if(popup.arrowCanvas){
                            popup.arrowCanvas.remove();
                            popup.arrowCanvas = null;
                        }
                        beingShown = false;
                        shouldShow = false;
                        $(document).trigger("hideLayer", ["inlineDialog", getHash()]);
                        AJS.InlineDialog.current = null;
                        if (!opts.cacheContent) {
                            //if not caching the content, then reset the
                            //flags to false so as to reload the content
                            //on next mouse hover.
                            contentLoaded = false;
                            contentLoading = false;
                        }

                    }, delay);
                }

            }
        };

        // the trigger is the jquery element that is triggering the popup (i.e., the element that the mousemove event is bound to)
        var initPopup = function(e,trigger) {
            var $trigger = $(trigger);

            opts.upfrontCallback.call({
                popup: popup,
                hide: function () {hidePopup(0);},
                id: identifier,
                show: function () {showPopup();}
            });

            popup.each(function() {
                if (typeof this.popup != "undefined") {
                    this.popup.hide();
                }
            });
            
            //Close all other popups if neccessary
            if (opts.closeOthers) {
                $(".aui-inline-dialog").each(function() {
                    !this.popup.persistent && this.popup.hide();
                });
            }
            
            //handle programmatic showing where there is no event
            targetPosition = {target: $trigger};
            if (!e) {
                mousePosition = { x: $trigger.offset().left, y: $trigger.offset().top };
            } else {
                mousePosition = { x: e.pageX, y: e.pageY };
            }
            


            if (!beingShown) {
                clearTimeout(showTimer);
            }
            shouldShow = true;
            var doShowPopup = function() {
                contentLoading = false;
                contentLoaded = true;
                opts.initCallback.call({
                    popup: popup,
                    hide: function () {hidePopup(0);},
                    id: identifier,
                    show: function () {showPopup();}
                });
                showPopup();
            };
            // lazy load popup contents
            if (!contentLoading) {
                contentLoading = true;
                if ($.isFunction(url)) {
                    // If the passed in URL is a function, execute it. Otherwise simply load the content.
                    url(contents, trigger, doShowPopup);
                } else {
                    //Retrive response from server
                    $.get(url, function (data, status, xhr) {
                        //Load HTML contents into the popup
                        contents.html(opts.responseHandler(data, status, xhr));
                        //Show the popup
                        contentLoaded = true;
                        opts.initCallback.call({
                            popup: popup,
                            hide: function () {hidePopup(0);},
                            id: identifier,
                            show: function () {showPopup();}
                        });
                        showPopup();
                    });
                }
            }
            // stops the hide event if we move from the trigger to the popup element
            clearTimeout(hideDelayTimer);
            // don't trigger the animation again if we're being shown
            if (!beingShown) {
                showPopup();
            }
            return false;
        };

        popup[0].popup = getHash();

        var contentLoading = false;
        var added  = false;
        var appendPopup = function () {
            if (!added) {
                $(opts.container).append(popup);
                added = true;
            }
        };
        if (opts.onHover) {
            if (opts.useLiveEvents) {
                $(document).on("mousemove", items, function(e) {
                    appendPopup();
                    initPopup(e,this);
                }).on("mouseout", items, function(e) {
                    hidePopup();
                });
            } else {
                $(items).mousemove(function(e) {
                    appendPopup();
                    initPopup(e,this);
                }).mouseout(function() {
                    hidePopup();
                });
            }
        } else {
            if (!opts.noBind) {   //Check if the noBind option is turned on
                if (opts.useLiveEvents) {
                    $(document).on("click", items, function(e) {
                        appendPopup();
                        initPopup(e,this);
                        return false;
                    }).on("mouseout", items, function(){
                        hidePopup();
                    });
                } else {
                    $(items).click(function(e) {
                        appendPopup();
                        initPopup(e,this);
                        return false;
                    }).mouseout(function() {
                        hidePopup();
                    });
                }
            }
        }

        // Be defensive and make sure that we haven't already bound the event
        var hasBoundOnExternalClick = false;
        var externalClickNamespace = identifier + ".inline-dialog-check";

        /**
         * Catch click events on the body to see if the click target occurs outside of this popup
         * If it does, the popup will be hidden
         */
        var bindHideOnExternalClick = function () {
            if (!hasBoundOnExternalClick) {
                $("body").bind("click." + externalClickNamespace, function(e) {
                    var $target = $(e.target);
                    // hide the popup if the target of the event is not in the dialog
                    if ($target.closest('#inline-dialog-' + identifier + ' .contents').length === 0) {
                        hidePopup(0);
                    }
                });
                hasBoundOnExternalClick = true;
            }
        };

        var unbindHideOnExternalClick = function () {
            if (hasBoundOnExternalClick) {
                $("body").unbind("click." + externalClickNamespace);
            }
            hasBoundOnExternalClick = false;
        };

        /**
         * Show the inline dialog.
         * @method show
         */
        popup.show = function (e) {
            if (e) {
                e.stopPropagation();
            }
            appendPopup();
            initPopup(null, items);
        };
        /**
         * Hide the inline dialog.
         * @method hide
         */
        popup.hide = function () {
            hidePopup(0);
        };
        /**
         * Repositions the inline dialog if being shown.
         * @method refresh
         */
        popup.refresh = function () {
            if (beingShown) {
               getHash().reset(); 
            }
        };
        
        popup.getOptions = function(){
            return opts;
        };
        
        return popup;
    };

    function dimensionsOf(el) {
        var $el = $(el);
        var offset = $.extend({left: 0, top: 0}, $el.offset());
        return {
            left: offset.left,
            top: offset.top,
            width: $el.outerWidth(),
            height: $el.outerHeight()
        }
    }

    AJS.InlineDialog.opts = {
        onTop: false,
        responseHandler: function(data, status, xhr) {
            //assume data is html
            return data;
        },
        closeOthers: true,
        isRelativeToMouse: false,
        addActiveClass: true, // if false, signifies that the triggers should not have the "active" class applied
        onHover: false,
        useLiveEvents: false,
        noBind: false,
        fadeTime: 100,
        persistent: false,
        hideDelay: 10000,
        showDelay: 0,
        width: 300,
        offsetX: 0,
        offsetY: 10,
        arrowOffsetX: 0,
        container: "body",
        cacheContent : true,
        displayShadow: true,
        preHideCallback: function () { return true; },
        hideCallback: function(){}, // if defined, this method will be exected after the popup has been faded out.
        initCallback: function(){}, // A function called after the popup contents are loaded. `this` will be the popup jQuery object, and the first argument is the popup identifier.
        upfrontCallback: function() {}, // A function called before the popup contents are loaded. `this` will be the popup jQuery object, and the first argument is the popup identifier.
        /**
         * Returns an object with the following attributes:
         *      popupCss css attributes to apply on the popup element
         *      arrowCss css attributes to apply on the arrow element
         *
         * @param popup
         * @param targetPosition position of the target element
         * @param mousePosition current mouse position
         * @param opts options
         */
        calculatePositions: function (popup, targetPosition, mousePosition, opts) {
            opts = opts || {};
            var viewportDimensions = dimensionsOf(window);
            var targetDimensions = dimensionsOf(targetPosition.target);
            var popupDimensions = dimensionsOf(popup);
            var arrowDimensions = dimensionsOf(popup.find(".arrow"));

            var middleOfTrigger = targetDimensions.left + targetDimensions.width/2; //The absolute x position of the middle of the Trigger
            var bottomOfViewablePage = (window.pageYOffset || document.documentElement.scrollTop) + viewportDimensions.height;
            var SCREEN_PADDING = 10; //determines how close to the edge the dialog needs to be before it is considered offscreen

            // Set popup's position (within the viewport)
            popupDimensions.top = targetDimensions.top + targetDimensions.height + ~~opts.offsetY;
            popupDimensions.left = targetDimensions.left + ~~opts.offsetX;

            // Calculate if the popup would render off the side of the viewport
            var diff = viewportDimensions.width - (popupDimensions.left + popupDimensions.width + SCREEN_PADDING);

            // Set arrow's position (within the popup)
            arrowDimensions.left = middleOfTrigger - popupDimensions.left + ~~opts.arrowOffsetX;
            arrowDimensions.top = -(arrowDimensions.height/2);

            // Check whether the popup should display above or below the trigger
            var enoughRoomAbove = targetDimensions.top > popupDimensions.height;
            var enoughRoomBelow = (popupDimensions.top + popupDimensions.height) < bottomOfViewablePage;
            var displayAbove = (!enoughRoomBelow && enoughRoomAbove) || (enoughRoomAbove && opts.onTop);

            if (displayAbove) {
                popupDimensions.top = targetDimensions.top - popupDimensions.height - (arrowDimensions.height/2);
                arrowDimensions.top = popupDimensions.height;
            }

            // Check if the popup should show up relative to the mouse
            if(opts.isRelativeToMouse){
                if(diff < 0){
                    popupDimensions.right = SCREEN_PADDING;
                    popupDimensions.left = "auto";
                    arrowDimensions.left = mousePosition.x - (viewportDimensions.width - popupDimensions.width);
                }else{
                    popupDimensions.left = mousePosition.x - 20;
                    arrowDimensions.left = mousePosition.x - popupDimensions.left;
                }
            }else{
                if(diff < 0){
                    popupDimensions.right = SCREEN_PADDING;
                    popupDimensions.left = "auto";

                    var popupRightEdge = viewportDimensions.width - popupDimensions.right;
                    var popupLeftEdge = popupRightEdge - popupDimensions.width;

                    //arrow's position must be relative to the popup's position and not of the screen.
                    arrowDimensions.right = "auto";
                    arrowDimensions.left = middleOfTrigger - popupLeftEdge - arrowDimensions.width/2;
                } else if(popupDimensions.width <= targetDimensions.width/2){
                    arrowDimensions.left = popupDimensions.width/2;
                    popupDimensions.left = middleOfTrigger - popupDimensions.width/2;
                }
            }
            return {
                displayAbove: displayAbove,
                popupCss: {
                    left: popupDimensions.left,
                    top: popupDimensions.top,
                    right: popupDimensions.right
                },
                arrowCss: {
                    left: arrowDimensions.left,
                    top: arrowDimensions.top,
                    right: arrowDimensions.right
                }
            }
        },
        getArrowPath : function (positions) {
            return positions.displayAbove ?
                "M0,8L8,16,16,8" :
                "M0,8L8,0,16,8"
        },
        getArrowAttributes: function () {
            return {
                fill : "#fff",
                stroke : "#ccc"
            };
        }
    };
})(AJS.$);
