Qunit.require('js/external/html5-shim/html5.js');
Qunit.require('js/external/jquery/jquery.js');
Qunit.require('js/atlassian/atlassian.js');

module("HTML5shim Unit Tests");

test("html5 support / shim test in IE8 and below", function() {

    // check section tag has display block.
    var html5message,
        whatWeWant = "block",
        theBackground = AJS.$("#html5").css("display");

    if ( AJS.$.browser.msie && parseInt(AJS.$.browser.version) < 9 ) {
        html5message = " HTML5 elements should be supported by shim. ";
    } else {
        html5message = " HTML5 elements should be natively supported. ";
    }

    equal(theBackground, whatWeWant, html5message);

});