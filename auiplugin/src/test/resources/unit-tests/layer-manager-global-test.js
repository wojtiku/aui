Qunit.require('js/external/jquery/jquery.js');
Qunit.require('js/atlassian/atlassian.js');
Qunit.require('js/atlassian/keyCode.js');
Qunit.require('js/atlassian/internal/widget.js');
Qunit.require('js/atlassian/layer.js');
Qunit.require('js/atlassian/layer-manager.js');
Qunit.require('js/atlassian/layer-manager-global.js');

module("Layer manager global tests", {
    setup: function() {
        this.dimSpy = AJS.dim = sinon.spy();
        this.undimSpy = AJS.undim = sinon.spy();
        this.layerManagerPopTopSpy = sinon.stub(AJS.LayerManager.global, "popTop");
        this.layerManagerPopUntilTopBlanketedSpy = sinon.stub(AJS.LayerManager.global, "popUntilTopBlanketed");
        AJS.FocusManager = {
            global: {
                enter: sinon.spy(),
                exit: sinon.spy()
            }
        };
    },
    teardown: function() {
        // AJS.layer.show() moves the element to be a child of the body (not under #qunit-fixture), so clean up popups
        AJS.$(".aui-layer").remove();
        this.layerManagerPopTopSpy.restore();
        this.layerManagerPopUntilTopBlanketedSpy.restore();
    },
    createLayer: function(blanketed) {
        var $el = AJS.$("<div></div>").addClass("aui-layer").attr("aria-hidden", "true").css("position", "absolute").appendTo("#qunit-fixture");
        if (blanketed) {
            $el.attr("data-aui-blanketed", "true");
        }
        return $el;
    },
    pressEsc: function() {
        var e = jQuery.Event("keydown");
        e.keyCode = AJS.keyCode.ESCAPE;
        $(document).trigger(e);
    },
    clickBlanket: function() {
        // We don't want to include blanket.js - which creates the blanket - in our dependencies,
        // so create a mock blanket element
        var $blanket = AJS.$("<div></div>").addClass("aui-blanket").appendTo("#qunit-fixture");
        $blanket.click();
    }
});

test("Pressing ESC calls popTop", function() {
    var $el = this.createLayer();
    AJS.LayerManager.global.push($el);

    this.pressEsc();

    ok(this.layerManagerPopTopSpy.calledOnce, "Expected popUntil called once");
});

test("Clicking blanket calls popUntilTopBlanketed", function() {
    var $el = this.createLayer(true);
    AJS.LayerManager.global.push($el);

    this.clickBlanket();

    ok(this.layerManagerPopUntilTopBlanketedSpy.calledOnce, "Expected popUntil called once");
});