Qunit.require('js/external/jquery/jquery.js');
Qunit.require('js/external/jquery/jquery-ui/jquery-ui.js');
Qunit.require('js/external/underscorejs/underscore.js');
Qunit.require('js/atlassian/aui-experimental-sidebar.js');

module("ui.sidebar", {
    setup: function () {
        this.$styleRule = jQuery("<style type='text/css'> .ui-sidebar{ position: fixed;width: 15px;} </style>").appendTo("head");
        this.$el = jQuery("<div />").css({position: "absolute", left: 0, top:0, width: 200}).text('Lots of text that will wrap and change the height of the container when the sidebar is re-sized').appendTo("body");
        this.clock = sinon.useFakeTimers();
    },
    teardown: function () {
        window.localStorage.removeItem("ui.sidebar.sidebar-test");
        this.$styleRule.remove();
        this.$el.remove();
        this.clock.restore();
    }
});

test("Error is provided if no id is set", function () {
    console.error = sinon.spy();
    this.$el.sidebar();
     QUnit.equal(console.error.callCount, 1, "Expected error to be thrown");
});

test("Mousedown sets correct containment for handle", function () {

    var minWidth = 100;
    var maxWidth = 500;

    this.$el.css({position: "absolute", left: 0});

    this.$el.sidebar({
        id: "sidebar-test",
        minWidth: function () {
            return minWidth;
        },
        maxWidth: function () {
            return maxWidth;
        }
    });
    var $handle = this.$el.find(".ui-sidebar");
    $handle.trigger("mousedown");
    var containment = $handle.draggable("option", "containment");
    var x1 = containment[0];
    var x2 = containment[2];

     QUnit.equal(x1, minWidth);
     QUnit.equal(x2, maxWidth);
});

test("Setting containment takes into account element offset", function () {

    var minWidth = 100;
    var maxWidth = 500;
    var elOffset = 50;

    this.$el.css({position: "absolute", left: elOffset});

    this.$el.sidebar({
        id: "sidebar-test",
        minWidth: function () {
            return minWidth;
        },
        maxWidth: function () {
            return maxWidth;
        }
    });
    var $handle = this.$el.find(".ui-sidebar");
    $handle.trigger("mousedown");
    var containment = $handle.draggable("option", "containment");
    var x1 = containment[0];
    var x2 = containment[2];

     QUnit.equal(x1, minWidth + elOffset);
     QUnit.equal(x2, maxWidth + elOffset);

    this.$el.css({left: elOffset * 2});

    $handle.trigger("mousedown");
    var containment = $handle.draggable("option", "containment");
    var x1 = containment[0];
    var x2 = containment[2];

     QUnit.equal(x1, minWidth + (elOffset * 2));
     QUnit.equal(x2, maxWidth + (elOffset * 2));
});

test("Setting of handler position on init", function () {
    this.$el.css({width: 10 }).sidebar({id: "sidebar-test"});
    var $handle = this.$el.find(".ui-sidebar");
     QUnit.equal($handle.css("left"), "10px");
});

test("Updating position causes handle to be repositioned", function () {
    this.$el.sidebar({id: "sidebar-test"});
    this.$el.css({"left": 100});
    this.$el.sidebar("updatePosition");
    var $handle = this.$el.find(".ui-sidebar");
     QUnit.equal($handle.offset().left, 300);
});

test("Updating position updates width", function () {
    this.$el.width(200).sidebar({id: "sidebar-test", minWidth: function () {return 100;}});
    this.$el.css({"width": 50});
    this.$el.sidebar("updatePosition");
     QUnit.equal(this.$el.width(), 100, "element should be updated to reflect minimum width");
});

test("Window resize updates width", function () {
    this.$el.width(200).sidebar({id: "sidebar-test", minWidth: function () {return 100;}});
    this.$el.css({"width": 50});
    jQuery(window).trigger("resize");
    this.clock.tick(200); // this functionality is debounced so we need to move forward in time
     QUnit.equal(this.$el.width(), 100, "element should be updated to reflect minimum width");
});

test("Handle always stretches to full-height of container", function () {
    var $handle;

    this.$el.width(200).sidebar({id: "sidebar-test", minWidth: function () {return 100;}});

    $handle = this.$el.find(".ui-sidebar");

    this.$el.css({"width": 100});
    this.$el.sidebar("updatePosition");
    this.clock.tick(200); // this functionality is debounced so we need to move forward in time
    QUnit.equal(this.$el.height(), $handle.height(), "handle should always be same height as container");
});